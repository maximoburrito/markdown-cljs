(ns app.misc)

(defmacro text-file [file]
  (slurp (format "resources/md/%s" file)))
