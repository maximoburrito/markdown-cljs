(ns app.ui.directives
  (:require [app.ui.shared :as shared]
            [clojure.string :as string]
            [clojure.pprint :as pprint]
            [com.fulcrologic.fulcro.application :as app]
            [com.fulcrologic.fulcro.algorithms.react-interop :as react-interop]
            [com.fulcrologic.fulcro.algorithms.denormalize :as fdn]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]
            ;;["node-emoji" :as emoji]
            ["react-markdown" :as markdown]
            ["remark-directive$default" :as remarkDirective]
            ["unist-util-visit" :refer [visit]]
            [clojure.string :as str])
  (:require-macros [app.misc :refer [text-file]]))


(def ui-markdown (react-interop/react-factory markdown/default))

(defn icon-directive [options]
  (fn [tree]
    (visit tree
           (fn [node]
             (println node)
             (and (= "textDirective" (.-type node))
                  (= "icon" (.-name node))))
           (fn [node]
             (set! (.-data node)
                   (clj->js {:hName "icon"
                             :hProperties {:className (str/join " " (keys (js->clj (.-attributes node))))}
                             :hChildren []}))))))

(defn label-directive [options]
  (fn [tree]
    (visit tree
           (fn [node]
             (println node)
             (and (= "textDirective" (.-type node))
                  (= "label" (.-name node))))
           (fn [node]
             (set! (.-data node)
                   (clj->js {:hName "label"
                             :hProperties {:className (str/join " " (keys (js->clj (.-attributes node))))}
                             :hChildren (.-children node)}))))))

(defsc Directives [this {::keys [markdown-text use-icon use-label use-directives json-mode] :as props}]
  {:query         [::markdown-text ::use-directives ::use-icon ::use-label ::json-mode]
   :ident         (fn [] [:component/id :directives])
   :route-segment ["directives"]
   :initial-state {::markdown-text  (text-file "directives.md")
                   ::use-directives true
                   ::use-icon       true
                   ::use-label      true
                   ::json-mode      :Off}}

  (dom/div :.ui.container.segment.raised
    (dom/div :.ui.huge.red.ribbon.label "remarkPlugins (directives)")
    (dom/div :.ui.form
      (dom/div :.ui.padded.grid
        (dom/div :.ui.row
          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::use-directives))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "directives")
              (dom/a :.ui.basic.label (if use-directives "On" "Off"))))


          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::use-icon))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "icons")
              (dom/a :.ui.basic.label (if use-icon "On" "Off"))))

          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::use-label))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "labels")
              (dom/a :.ui.basic.label (if use-label "On" "Off"))))

          (dom/div :.ui.field
            {:onClick (fn [e]
                        (m/set-value! this ::json-mode (case json-mode
                                                         :Off   :MDAST
                                                         :MDAST :HAST
                                                         :HAST  :Off)))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "JSON")
              (dom/a :.ui.basic.label (name json-mode))))))

      (dom/textarea :.ui.input.field
                    {:rows        10
                     :value       markdown-text
                     :placeholder "markdown"
                     :onChange    (fn [e]
                                    (m/set-string! this ::markdown-text :event e))}))

    (dom/div :.ui.hidden.divider)
    (dom/div :.ui.header.large.red.ribbon.label "Output" )
    (dom/div :.ui.hidden.divider)

    (ui-markdown {:components {:code  shared/highlight-code
                               :pre   #(dom/pre :.ui.info.message (.-children %))
                               :icon  #(dom/i :.icon %)
                               :label #(dom/div :.ui.label
                                         {:className (.-className %)}
                                         (.-children %))
                               :del   #(dom/div :.ui.teal.label (dom/del (.-children %)))}

                  :remarkPlugins (cond-> [  shared/remove-position]
                                   use-directives       (conj remarkDirective)
                                   use-icon             (conj icon-directive)
                                   use-label            (conj label-directive)
                                   (= :MDAST json-mode) (conj shared/mdast-json))
                  :rehypePlugins (when (= :HAST json-mode) [shared/hast-json])}
                 markdown-text)))

