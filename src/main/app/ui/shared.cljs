(ns app.ui.shared
  (:require [com.fulcrologic.fulcro.dom :as dom]
            ["highlight.js" :as hljs]
            ["unist-util-remove-position" :refer [removePosition]]
            ["unist-util-visit" :refer [visit]]))


(defn highlight-code [node]
  (dom/code
   (let [child (first (.-children node))
          html (.-value (.highlightAuto hljs child))]
      ;; you didn't see this... SHHH!
      {:dangerouslySetInnerHTML {:__html html}})))


(defn mdast-json [options]
  (fn [tree]
    (clj->js
      {:type "root"
       :children [{:type "code"
                   :value (js/JSON.stringify tree nil 2 )}]})))

(defn hast-json [options]
  (fn [tree]
    (clj->js
      {:type "root"
       :children [{:type "element"
                   :tagName "pre"
                   :properties {}
                   :children [{:type "element"
                               :tagName "code"
                               :properties {}
                               :children [{:type "text"
                                           :value (js/JSON.stringify tree nil 2 )}]}]}]})))


(defn remove-position [options]
  removePosition)
