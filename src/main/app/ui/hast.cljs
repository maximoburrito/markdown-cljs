(ns app.ui.hast
  (:require [app.ui.shared :as shared]
            [clojure.string :as string]
            [com.fulcrologic.fulcro.algorithms.react-interop :as react-interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]
            ["react-markdown" :as markdown]
            ["unist-util-visit" :refer [visit]]))

(def ui-markdown (react-interop/react-factory markdown/default))


(defn reverse-text [options]
  (fn [tree]
    (visit tree "text"
      (fn [node]
        (set! (.-value node) (apply str (reverse (.-value node))))))))


(defsc HAST [this {::keys [markdown-text use-reverse use-json]}]
  {:query         [::markdown-text ::use-reverse ::use-json]
   :ident         (fn [] [:component/id :hast])
   :route-segment ["hast"]
   :initial-state {::markdown-text "# This is some text\na pdf here\n"
                   ::use-json false
                   ::use-reverse true}}

  (dom/div :.ui.container.segment.raised
    (dom/div :.ui.huge.red.ribbon.label "rehypePlugins (HAST)")


    (dom/div :.ui.form
      (dom/div :.ui.padded.grid
        (dom/div :.ui.row
          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::use-reverse))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "Reverse")
              (dom/a :.ui.basic.label (if use-reverse "On" "Off"))))
          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::use-json))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "Show")
              (dom/a :.ui.basic.label (if use-json "JSON" "HTML")))))

        (dom/textarea :.ui.input.field
                      {:rows 10
                       :value markdown-text
                       :placeholder "markdown"
                       :onChange (fn [e]
                                   (m/set-string! this ::markdown-text :event e))})))

    (dom/div :.ui.hidden.divider)
    (dom/div :.ui.header.large.red.ribbon.label "Output" )
    (dom/div :.ui.hidden.divider)


    (ui-markdown {:components {:code shared/highlight-code
                               :pre #(dom/pre :.ui.info.message (.-children %))}
                  :rehypePlugins (concat [shared/remove-position]
                                         (when use-reverse [reverse-text])
                                         (when use-json [shared/hast-json]))}
                 markdown-text)))
