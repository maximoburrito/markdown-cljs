(ns app.ui.gfm
  (:require [app.ui.shared :as shared]
            [clojure.string :as string]
            [com.fulcrologic.fulcro.algorithms.react-interop :as react-interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.mutations :as m]
            ["react-markdown" :as markdown]
            ["remark-gfm$default" :as remark-gfm])
    (:require-macros [app.misc :refer [text-file]]))

(def ui-markdown (react-interop/react-factory markdown/default))


(defn transform-json [options]
  (fn [tree]
    (clj->js
      {:type "root"
       :children [{:type "element"
                   :tagName "pre"
                   :properties {}
                   :children [{:type "text"
                               :value (js/JSON.stringify tree nil 2 )}]}]})))

(defsc GFM [this {::keys [markdown-text use-gfm use-json]}]
  {:query         [::markdown-text ::use-gfm ::use-json]
   :ident         (fn [] [:component/id :gfm])
   :route-segment ["gfm"]
   :initial-state {::markdown-text (text-file "gfm.md")
                   ::use-gfm       true
                   ::use-json      true}}


  (dom/div :.ui.container.segment.raised
    (dom/div :.ui.huge.red.ribbon.label "remarkPlugins (GFM)")

    (dom/div :.ui.form.padded.grid.divided
      (dom/div :.ui.row
        (dom/div :.ui.field
          {:onClick (fn [e] (m/toggle! this ::use-gfm))}
          (dom/div :.ui.labeled.button
            (dom/div :.ui.teal.button "Github Markdown")
            (dom/a :.ui.basic.label (if use-gfm "On" "Off"))))

        (dom/div :.ui.field
          {:onClick (fn [e] (m/toggle! this ::use-json))}
          (dom/div :.ui.labeled.button
            (dom/div :.ui.teal.button "Show")
            (dom/a :.ui.basic.label (if use-json "JSON" "HTML")))))

      (dom/textarea :.ui.input.field
                    {:rows        10
                     :value       markdown-text
                     :placeholder "markdown"
                     :onChange    (fn [e]
                                    (m/set-string! this ::markdown-text :event e))}))

    (dom/div :.ui.hidden.divider)
    (dom/div :.ui.header.large.red.ribbon.label "Output" )
    (dom/div :.ui.hidden.divider)

    (ui-markdown {:components {:code      shared/highlight-code
                               :pre       #(dom/pre :.ui.info.message (.-children %))
                               :table     #(dom/table :.ui.teal.table
                                                      ;; ignoring .-align
                                                      (.-children %))
                               :tableRow  #(dom/tr (.-children %))
                               :tableCell #(dom/td (.-children %))
                               :ul #(dom/ul :.ui.list (.-children %))
                               :li #(if (nil? (.-checked %))
                                      (dom/li (.-children %))
                                      (dom/div :.field
                                       (dom/div :.ui.togglex.checkbox
                                         (dom/input {:type "checkbox" :checked (.-checked %)})
                                         (dom/label (.-children %)))))

                               :del #(dom/div :.ui.yellow.label (dom/del (.-children %)))}

                  :remarkPlugins (cond-> [shared/remove-position]
                                   use-gfm (conj remark-gfm)
                                   false   (conj shared/mdast-json))
                  :rehypePlugins (cond-> []
                                   use-json (conj shared/hast-json))}
                 markdown-text)))
