(ns app.ui.basic
  (:require [com.fulcrologic.fulcro.algorithms.react-interop :as interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.mutations :as m]
            ["react-markdown" :as markdown]))

(def ui-markdown (interop/react-factory markdown/default))

(defsc Basic [this {::keys [markdown-text use-components]}]
  {:ident         (fn [] [:component/id :basic])
   :route-segment ["basic"]
   :query         [::markdown-text ::use-components]
   :initial-state {::markdown-text "# this is some text\n```\nhello\n```"
                   ::use-components true}}

  (dom/div :.ui.container.segment.raised
    (dom/div :.ui.huge.red.ribbon.label "Basic Formatting")

    (dom/div :.ui.form
      (dom/div :.ui.padded.grid

        (dom/div :.ui.row
          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::use-components))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "Components")
              (dom/a :.ui.basic.label (if use-components "ON" "OFF")))))

        (dom/textarea :.ui.input.field
                      {:rows 10
                       :value markdown-text
                       :placeholder "markdown"
                       :onChange (fn [e]
                                   (m/set-string! this ::markdown-text :event e))})))

    (dom/div :.ui.hidden.divider)
    (dom/div :.ui.header.large.red.ribbon.label "Output" )
    (dom/div :.ui.hidden.divider)


    (ui-markdown {:components
                  (when use-components
                    {:pre (fn [props]
                            (dom/span {:style {:color "red"}}
                              (.-children props)))
                     :h1 #(dom/div :.ui.huge.header
                            (dom/i :.icon.angle.right)
                            (dom/div :.content (.-children %)))
                     :h2 #(dom/div :.ui.large.header (.-children %))
                     :h3 #(dom/div :.ui.small.header (.-children %))
                     :ul #(dom/ul :.ui.list (.-children %))
                     :img #(dom/img :.ui.image.small.centered %)})
                  :children markdown-text})))
