(ns app.ui.home
  (:require [com.fulcrologic.fulcro.algorithms.react-interop :as react-interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            ["react-markdown" :rename {default react-markdown}])
  (:require-macros [app.misc :refer [text-file]]))

(def ui-markdown (react-interop/react-factory react-markdown))

(defn md->html [md] "<script>document.log('hi')</script>a")

(defsc Main [this {:keys [text]}]
  {:ident         (fn [] [:component/id :main])
   :route-segment ["main"]
   :query         [:text]
   :initial-state {:text (text-file "home.md")}}

  (dom/div :.ui.container.segment
    #_(dom/p {:dangerouslySetInnerHTML {:__html (md->html text)}})
    (ui-markdown {:children text})))
