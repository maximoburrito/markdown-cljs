(ns app.ui.ast
  (:require [com.fulcrologic.fulcro.algorithms.react-interop :as interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.mutations :as m]
            [app.ui.shared :as shared]
            ["react-markdown" :as markdown]))

(def ui-markdown (interop/react-factory markdown/default))

(defsc AST [this {::keys [markdown-text hast]}]
  {:ident         (fn [] [:component/id :ast])
   :route-segment ["ast"]
   :query         [::markdown-text ::hast]
   :initial-state {::markdown-text "# this is some text"
                   ::hast true}}

  (dom/div :.ui.container.segment
    (dom/div :.ui.huge.red.ribbon.label "AST inspect")

    (dom/div :.ui.form
      (dom/div :.ui.padded.grid
        (dom/div :.ui.row
          (dom/div :.ui.field
            {:onClick (fn [e] (m/toggle! this ::hast))}
            (dom/div :.ui.labeled.button
              (dom/div :.ui.teal.button "Show")
              (dom/a :.ui.basic.label (if hast "HAST" "MDAST")))))

        (dom/textarea
         :.ui.input.field
         {:rows 10
          :value markdown-text
          :placeholder "markdown"
          :onChange (fn [e]
                      (m/set-string! this ::markdown-text :event e))})))

    (dom/div :.ui.hidden.divider)
    (dom/div :.ui.header.large.red.ribbon.label "Output" )
    (dom/div :.ui.hidden.divider)

    (ui-markdown {:components {:code shared/highlight-code
                               :pre #(dom/pre :.ui.info.message (.-children %))}
                  :remarkPlugins (concat [shared/remove-position]
                                         (when (not hast) [shared/mdast-json]))
                  :rehypePlugins (when hast (list shared/hast-json))
                  :children markdown-text})))
