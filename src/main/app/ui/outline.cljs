(ns app.ui.outline
  (:require [app.ui.shared :as shared]
            [com.fulcrologic.fulcro.algorithms.react-interop :as interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            ["react-markdown" :as markdown])
  (:require-macros [app.misc :refer [text-file]]))

(def ui-markdown (interop/react-factory markdown/default))

(defsc Outline [this {::keys [markdown-text]}]
  {:ident         (fn [] [:component/id :outline])
   :query         [::markdown-text]
   :route-segment ["outline"]
   :initial-state {::markdown-text (text-file "outline.md")}}

  (dom/div :.ui.container.segment
    (ui-markdown {:components
                  {:pre #(dom/pre :.ui.teal.message (.-children %))
                   :code shared/highlight-code
                   :h1 #(dom/h2 :.ui.horizontal.divider.header
                                (.-children %))
                   :h2 #(dom/div :.ui.blue.ribbon.label
                                {:style {:marginTop "40px"}}
                                (.-children %))

                   :h3 #(dom/h3 :.ui.header (.-children %))
                   :ul #(dom/ul :.ui.list (.-children %))
                   :img #(dom/img :.ui.image.small.centered %)}}
                 markdown-text)))
