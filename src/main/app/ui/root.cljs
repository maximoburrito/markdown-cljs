(ns app.ui.root
  (:require [app.ui.directives :as directives]
            [app.ui.ast :as ast]
            [app.ui.home :as home]
            [app.ui.basic :as basic]
            [app.ui.outline :as outline]
            [app.ui.gfm :as gfm]
            [app.ui.hast :as hast]
            [app.ui.pdf :as pdf]
            [clojure.string :as str]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.application :as app]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.routing.dynamic-routing :as dr]
            [taoensso.timbre :as log]))

(dr/defrouter TopRouter [this props]
  {:router-targets [home/Main
                    outline/Outline
                    basic/Basic
                    ast/AST
                    pdf/PDF
                    hast/HAST
                    gfm/GFM
                    directives/Directives]})

(def ui-top-router (comp/factory TopRouter))

(defsc TopChrome [this {:root/keys [router]}]
  {:query         [{:root/router (comp/get-query TopRouter)}]
   :ident         (fn [] [:component/id :top-chrome])
   :initial-state {:root/router          {}}}
  (let [current-tab (or (first (dr/current-route this this)) "main") ]
    (dom/div :.ui.container
      (dom/div :.ui.pointing.menu
        (for [tab ["Main" "Outline" "Basic" "AST" "PDF" "HAST" "GFM" "Directives"]
              :let [tab-id (str/lower-case tab)]]
          (dom/a :.item
            {:key tab-id
             :classes [(when (= tab-id current-tab) "active")]
             :onClick (fn [] (dr/change-route this [tab-id]))}
            tab)))
      (dom/div :.ui.grid
        (dom/div :.ui.row
          (ui-top-router router))))))

(def ui-top-chrome (comp/factory TopChrome))

(defsc Root [this {:root/keys [top-chrome]}]
  {:query         [{:root/top-chrome (comp/get-query TopChrome)}]
   :initial-state {:root/top-chrome {}}}
  (ui-top-chrome top-chrome))
