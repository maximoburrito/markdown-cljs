(ns app.ui.pdf
  (:require [com.fulcrologic.fulcro.algorithms.react-interop :as react-interop]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]
            [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]
            ["react" :as react]
            ["react-pdf" :refer [Document Page]]
            ["react-markdown" :as markdown]))

(def ui-markdown (react-interop/react-factory markdown/default))
(def pdf-document (react-interop/react-factory Document))
(def pdf-page (react-interop/react-factory Page))

(defsc PDF [this {::keys [markdown-text]}]
  {:query         [::markdown-text]
   :ident         (fn [] [:component/id :pdf])
   :route-segment ["pdf"]
   :initial-state {::markdown-text "# this is some pdf text\na pdf here\n[super burrito](https://media-cdn.getbento.com/accounts/2970e43d5b7cf921de82c5631871d48f/media/anZWWTbnSjBQCxgHicgo_5th%20St.%20Menu%20IM%207.8.19.pdf)\n\n[cabo bobs](https://cabobobs.com/wp-content/uploads/2021/11/Catering-Menu-Nov-20211.pdf)
"}}

  (dom/div :.ui.container.segment.raised
    (dom/div :.ui.huge.red.ribbon.label "PDF")

    (dom/div :.ui.form
      (dom/div :.ui.padded.grid
        (dom/div :.ui.row
          (dom/textarea :.ui.input.field
                        {:rows        10
                         :value       markdown-text
                         :placeholder "markdown"
                         :onChange    #(m/set-string! this ::markdown-text :event %)}))))

    (dom/div :.ui.hidden.divider)
    (dom/div :.ui.header.large.red.ribbon.label "Output" )
    (dom/div :.ui.hidden.divider)

    (ui-markdown
     {:components #js {:p (fn [node]
                            (dom/div :.ui.segment
                              (.-children node)))
                       :a (fn [node]
                            (let [href             (.-href node)
                                  children         (.-children node)
                                  [pages setPages] (react/useState 0)]
                              (comp/fragment
                               (dom/div :.ui.header children (str "[" pages "]"))
                               (pdf-document {:className     "ui grid"
                                              :file          href
                                              :onLoadSuccess #(setPages (.-numPages %))}
                                             (for [n (range 1 (inc pages))]
                                               (dom/div :.ui.column.two {:style {:width "400px"}}
                                                 (pdf-page {:pageNumber n
                                                            :height     500})))))))}
      :children   markdown-text})))
