#  Quote of the day :icon{comment alternate outline}

:icon{small green left quote} _Programming Is Not About Typing, It's About Thinking_  :icon{small green right quote}

:label[Rich Hickey]{tag green}
