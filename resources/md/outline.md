# Markdown in ClojureScript

## The problem

Web applications take data from a database and put it on pages.
Email addresses. Product names. Prices.
Lots of it is simple data, but there's also a lot of structured text.

- We could break that text up into component data (inflexible)
- We could use HTML (bad security, easy to skip encoding, difficult to manage)
- Or a lighter markup like ... markdown?

## Markdown.. but what markdown? Aren't there LOTS of incompatable markdown?

The [original markdown](https://daringfireball.net/projects/markdown/syntax) is only loosely a spec. There are lots of variants, (github markdown, for example) but they are usually defined by an implementation - no compatability.
There is, however, actually now a good spec - [commonmark](https://spec.commonmark.org/0.30/)


## CommonMark

- unambiguous specification, with tests to verify implementations
- consistent with classic markdown while solving many markdown issues
- if you are using markdown and don't have a hard requirement for something else, use commonmark

## What are the options for clojure

- [https://github.com/commonmark/commonmark-java](https://github.com/commonmark/commonmark-java) java
- [https://github.com/vsch/flexmark-java](https://github.com/vsch/flexmark-java) java
- [https://github.com/bitterblue/commonmark-hiccup](https://github.com/bitterblue/commonmark-hiccup) uses commonmark-java to produce hiccup
- [https://github.com/remarkjs/react-markdown](https://github.com/remarkjs/react-markdown) javascript

There may be others, but these are the ones I can find that look commonmark based.
I'm interested in markdown in web applications, particularly react, so the choice for me is straightforward.

## Diversion - never dump HTML

You always want to use something that produces HTML through some sort of generation.
Never use non-HTML aware templates, don't use `dangerouslySetInnerHTML` or `{{somehtml}}`
Use virtual dom or hiccup or anything that produces valid, safe properly encoded HTML on your behalf
always


## react-markdown elevator pitch

- uses a well-defined spec (commonmark)
- plugin-based transformations (extensible, with a large community of off-the-shelf components)
- safe transformation into react virtual dom


## From the top, home.cljs

- a fulcro app, but there's nothing specific to fulcro
- using the fulcro template defaults, so there's a little cruft in there
- *react-interop/react-factory* is how you use a use a react component from fulcro component
- using `:children` because of fulcro and it's cleaner


This is very basic. Is it better than this?

```
(dom/p {:dangerouslySetInnerHTML {:__html (md->html markdown-text)}})
```

Yes, because it uses react components and not just raw HTML dumped into the page.


## A basic example (basic.cljs)

The most basic thing we we want to do is style our elements. Ignoring how react-markdown
works internally for the moment, we can pass mapping of HTML elements that would be output
and react DOM elements customized with out own style.

```
:ul #(dom/ul :.ui.list (.-children %))
```

There's a lot we can do here just with this, but before we get to that, let's learn a little
bit about how react-markdown works.

## unified ASTs (ast.cljs)

*react-markdown* uses remark, which is part of the unified ecosystem (https://unifiedjs.com/)
Unified is a framework for ASTs - abstract syntax trees.
ASTs are datastructures that represent the a parse tree of something


Markdown, for example: `*hello*`

```
{:type "root"
 :children [{:type "paragraph"
             :children [{:type "emphasis"
                         :children [{:type "text"
                                     :value "hello"}]}]}]}
```

unified defines a root node with a type, value, children and position field.
It doesn't defined what *type* is or what anything means
it provides tools for visiting and transforming trees (mutable though)

subprojects like remark (markdown) and rehype (html) exist
they define types and in some cases additional attributes. For example

```
{:type "heading"
 :depth 1
 :children [{:type "text" :value "hello"}]}
```


A remark (mdast) link would be

```
{:type "link"
 :title nil
 :url "https://www.meetup.com/Austin-Clojure-Meetup/"
 :children [{:type "text" :value "join"}]}
```


The rehype (hast) transformation might be

```
{:type "element"
 :tagName "a"
 :properties {:href "https://www.meetup.com/Austin-Clojure-Meetup/"}
 :children [{:type "text" :value "join"}]}
```

We could spit thouse out to HTML and slam it into the DOM, but a better solution is
to translate it into react virtual dom.

## custom components (pdf.js)

We saw earlier how to customize the presentation of elements, but the changes were
largely cosmetic. In the PDF example, I'm taking a set of PDF links and displaying
the individual pages, using [pdfjs](https://github.com/mozilla/pdf.js)
through [react-pdf](https://github.com/wojtekmaj/react-pdf).

- Can use any react component, not just make CSS changes
- Would this be better as data instead of markdown?
- What if the structure of your markdown HTML doesn't match the structure of components?


## transforming the AST (HAST.cljs)

A unified plugin is a transformer factory that returns a
transformation function. The transform function mutates the tree. A
chain of plugins works like a middleware stack except that the tree
is mutable. This leads to some messy code

```
(defn my-plugin [options]
  (fn [tree]
    ;; apply mutation to tree here
    ))
```

There are a number
[https://github.com/syntax-tree/unist#list-of-utilities](unist
utilities) for mutationt the tree. If trees were immutable, the core
Clojure libraries could accomplish nearly all these tasks.


Our very simple reverse-text transform uses [unist-util-visit](https://github.com/syntax-tree/unist-util-visit)
to find all _text_ nodes and reverses the string values.

```
(fn [tree]
  (visit tree "text"
    (fn [node]
      (set! (.-value node) (apply str (reverse (.-value node)))))))
```

These transforms can run on either the markdown AST or the HTML AST

## Markdown extensions (GFM.cljs)

This example uses [remark-gfm](https://github.com/remarkjs/remark-gfm) to add support for:

- automatic link literals
- strikethrough
- footnotes
- tables
- checkboxes

I don't really like the approach/assumptions made by some of this choices, but I do
regularly find myself using this extension.


## Directives (directives.cljs)

It's tempting to want to write parse extensions for markdown, but there's
a more generic, easier solution that I find often works well - directives.

This example uses [remark-directive](https://github.com/remarkjs/remark-directive), which
supports all three directive types:

- text directives, for inline text elements
- leaf directives, for block elements that don't have children
- container directives, for block elements that do have children


Basic syntax is one two or three colons, followed by:
- tag name
- `[text value]`, optional
- `{attributes}`, optional

Container directives will also have:
- children, any markdown
- closing colons



```
:::main{#readme}

Lorem:br
ipsum.

::hr{.red}

A :i[lovely] language know as :abbr[HTML]{title="HyperText Markup Language"}.

:::
```


