* Markdown in ClojureScript

This repository is a demo app for the Austin Clojure Meetup.


** running

#+begin_src
npx yarn
npx shadow-cljs compile main
clj -M:dev -m development
#+end_src

** details

The app will be running at http://localhost:3000/.

The *Outline* tab has the basic outline of the talk. (the closest
thing to slides) The other tabs have small examples illustrating
interesting markdown things.

** important links

- https://commonmark.org/
- https://unifiedjs.com/
- https://remark.js.org/
- https://github.com/rehypejs/rehype
- https://github.com/remarkjs/react-markdown
